# introlatex-xmind

#### 介绍
LaTeX使用简介及nwafuThesis西北农林科技大学学位论文模板讲座XMind思维导图。

### 使用说明
---------------------

1. 该思维导图基于XMind 2020实现。
2. 由于**绝对路径**问题，在不同电脑上单击文件链接时可能无法直接打开`*.tex`或`*.pdf`文件，此时可直接在**codes**文件夹中的对应子文件夹中找到并打开对应`*.tex`文件，或在**refs**文件夹中直接打开对应的`*.pdf`文件。
3. 所有网站链接仅保证当前工作有效，思维导图中不保持对这些链接的跟踪和维护。

###  参与贡献
---------------------
1. 本项目由西北农林科技大学信息工程学院耿楠创建和维护
2. 如果您愿意一同参与工作(不计报酬，免费自由)，请及时与作者联系
3. 如果您有任何改进意见或者功能需求，欢迎提交 [issue](https://gitee.com/nwafu_nan/introlatex-xmind/issues) 或 [pull request](https://gitee.com/nwafu_nan/introlatex-xmind/pulls)。
